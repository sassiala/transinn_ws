package com.transInn.transInnWebSite.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.transInn.transInnWebSite.model.devis;
public interface devisRepository extends JpaRepository<devis, Integer> {
}
